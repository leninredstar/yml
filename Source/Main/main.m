//
//  main.m
//  NIXProject
//
//  Created by Egor Zubkov on 1/22/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool
    {
#ifdef NIXENV_CI_BUILD
        DLOG(@"CI build enabled");
#endif
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
