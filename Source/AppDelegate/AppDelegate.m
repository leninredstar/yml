//
//  AppDelegate.m
//  NIXProject
//
//  Created by Egor Zubkov on 1/22/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//

#import "AppDelegate.h"
#import "Flurry.h"

@interface AppDelegate ()

- (void)setupFlurry;

@end

@implementation AppDelegate

@synthesize window = window_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self setupFlurry];
    
    return YES;
}

#pragma mark - Private

- (void)setupFlurry
{
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setSecureTransportEnabled:YES];
    [Flurry setShowErrorInLogEnabled:YES];
    [Flurry setDebugLogEnabled:YES];
    
#ifdef FLURRY_APP_KEY
    [Flurry startSession:FLURRY_APP_KEY];
#else
    #warning Please setup Flurry key in pch file
#endif
}

@end
